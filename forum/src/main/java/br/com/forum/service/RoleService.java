package br.com.forum.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.forum.model.Role;
import br.com.forum.repository.RoleRepository;


@Service
public class RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }
    
    public Role findByDescription(String description) {
        return roleRepository.findByDescription(description);
    }

    public Role create(Role role) {
        return roleRepository.save(role);
    }
    
    public Page<Role> findAll(Pageable pageable) { 
		return  roleRepository.findAll(pageable); 
	}
    public Page<Role> findByDescriptionPagination(String description , Pageable pageable) { 
		return  roleRepository.findByDescriptionPagination(description , pageable); 
	}
    
    public void delete(Role role) {
    	roleRepository.delete(role);
    }
   
    public Role findByRoleId(Integer id) { 
		return  roleRepository.findByRoleId(id);
	}
    
    public ResponseEntity<Object> deleteRole(Long id) {
        if(roleRepository.findById(id).isPresent()){
            if(roleRepository.getOne(id).getUsers() == null) {
                roleRepository.deleteById(id);
                if (roleRepository.findById(id).isPresent()) {
                    return ResponseEntity.unprocessableEntity().body("Failed to delete the specified record");
                } else return ResponseEntity.ok().body("Successfully deleted specified record");
            } else return ResponseEntity.unprocessableEntity().body("Failed to delete,  Please delete the users associated with this role");
        } else
            return ResponseEntity.unprocessableEntity().body("No Records Found");
    }

	
}
