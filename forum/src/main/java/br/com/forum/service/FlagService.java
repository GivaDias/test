package br.com.forum.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.forum.model.Flag;
import br.com.forum.repository.FlagRepository;


@Service
public class FlagService {

    private final FlagRepository flagRepository;

    @Autowired
    public FlagService(FlagRepository flagRepository) {
        this.flagRepository = flagRepository;
    }
    
    public Flag findByDescription(String description) {
        return flagRepository.findByDescription(description);
    }

    public Flag create(Flag flag) {
        return flagRepository.save(flag);
    }
    
    public Page<Flag> findAll(Pageable pageable) { 
		return  flagRepository.findAll(pageable); 
	}
    public Page<Flag> findByDescriptionPagination(String description , Pageable pageable) { 
		return  flagRepository.findByDescriptionPagination(description , pageable); 
	}
    
    public void delete(Flag flag) {
    	flagRepository.delete(flag);
    }
    
    public Flag findByFlagId(Integer id) { 
		return  flagRepository.findByFlagId(id);
	}
	
   
	
}
