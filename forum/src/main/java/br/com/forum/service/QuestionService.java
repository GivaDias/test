  package br.com.forum.service;
  
  
  import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.forum.model.Flag;
import br.com.forum.model.Question;
import br.com.forum.model.User;
import br.com.forum.repository.QuestionRepository;
import br.com.forum.repository.UserRepository;


  @Service
  public class QuestionService {

      private final QuestionRepository questionRepository;

      @Autowired
      public QuestionService(QuestionRepository questionRepository) {
          this.questionRepository = questionRepository;
      }
      
      @Autowired
      public UserRepository userRepository;  
      
       
      public Question findById(Long id) {
          return questionRepository.findByIdQuestion(id);
      }


      public Integer create(Question question) {
    	  
    	  Long idUser = null;
    	  for(User user :  question.getId_user()) {
    		  idUser= user.getId();
    	  }
    	  Long idFlag = null;
    	  for(Flag flag :  question.getId_flag()) {
    		  idFlag= flag.getId();
    	  }
    	    question.setCreatedAt(LocalDateTime.now());
    	    question.setResolved(false);
    	  	Integer intop =  questionRepository.saveQuestion(question.isResolved(), question.getCreatedAt(),
    	  			question.getComment(),idUser,idFlag);
		return intop;
      }
      
      public Question updateQuestion(Question question) {
    	  
    	  question.setUpdatedAt(LocalDateTime.now());
    	  question.setResolved(true);
    	  questionRepository.updateQuestion(question.isResolved(), 
    			  question.getUpdatedAt(), question.getComment(),  question.getId());
		return  question;
      }
      
      public Page<User> findAll(Pageable pageable) { 
  		return  userRepository.findAll(pageable); 
  	}
      public Page<User> findByNamePagination(String name , Pageable pageable) { 
    	  return userRepository.findByNamePagination(name, pageable);
  	}
      
      public Page<User> findByEmailPagination(String email , Pageable pageable) { 
    	  return userRepository.findByEmailPagination(email, pageable);
  	}
      public Page<User> findByBirthdatePagination(LocalDate birthdate , Pageable pageable) { 
    	  return userRepository.findByBirthdatePagination(birthdate, pageable);
  	}
      
      
      public Question findByQuestionId(Long id) { 
  		return  questionRepository.findByIdQuestion(id);
  	}
      
      public ResponseEntity<Object> deleteUser(Long id) {
          if (userRepository.findById(id).isPresent()) {
              userRepository.deleteById(id);
              if (userRepository.findById(id).isPresent())
                  return ResponseEntity.unprocessableEntity().body("Failed to Delete the specified User");
              else return ResponseEntity.ok().body("Successfully deleted the specified user");
          } else return ResponseEntity.badRequest().body("Cannot find the user specified");
      }

  
  }
