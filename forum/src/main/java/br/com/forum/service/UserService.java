  package br.com.forum.service;
  
  
  import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.forum.model.Role;
import br.com.forum.model.User;
import br.com.forum.repository.RoleRepository;
import br.com.forum.repository.UserRepository;


  @Service
  public class UserService {

      private final UserRepository userRepository;

      @Autowired
      public UserService(UserRepository userRepository) {
          this.userRepository = userRepository;
      }
      
      @Autowired
      public RoleRepository roleRepository;  
      
      
      public User findByName(String name) {
          return userRepository.findByName(name);
      }

      public User create(User user) {
    	   Role role = new Role();
    	   List<Role> roleList = new ArrayList<Role>();
		   User userNew = new User();
		   for(Role roleNew : user.getRoles()) {
			   role = roleRepository.findByDescription(roleNew.getDescription());
			   if(role == null) {	   
			   role = roleRepository.saveAndFlush(roleNew);
		   		}
			   roleList.add(role);
			   user.setRoles(roleList);
			   
		   }
		   userNew = userRepository.save(user);
		   
		return userNew;
      }
      
      public Page<User> findAll(Pageable pageable) { 
  		return  userRepository.findAll(pageable); 
  	}
      public Page<User> findByNamePagination(String name , Pageable pageable) { 
    	  return userRepository.findByNamePagination(name, pageable);
  	}
      
      public Page<User> findByEmailPagination(String email , Pageable pageable) { 
    	  return userRepository.findByEmailPagination(email, pageable);
  	}
      public Page<User> findByBirthdatePagination(LocalDate birthdate , Pageable pageable) { 
    	  return userRepository.findByBirthdatePagination(birthdate, pageable);
  	}
      
      
      public User findByUserId(Integer id) { 
  		return  userRepository.findByUserId(id);
  	}
      
      public ResponseEntity<Object> deleteUser(Long id) {
          if (userRepository.findById(id).isPresent()) {
              userRepository.deleteById(id);
              if (userRepository.findById(id).isPresent())
                  return ResponseEntity.unprocessableEntity().body("Failed to Delete the specified User");
              else return ResponseEntity.ok().body("Successfully deleted the specified user");
          } else return ResponseEntity.badRequest().body("Cannot find the user specified");
      }

  
  }
