/*
 * package br.com.forum.model;
 * 
 * import java.util.List;
 * 
 * import javax.persistence.CascadeType; import javax.persistence.Column; import
 * javax.persistence.Entity; import javax.persistence.GeneratedValue; import
 * javax.persistence.GenerationType; import javax.persistence.Id; import
 * javax.persistence.JoinColumn; import javax.persistence.ManyToMany;
 * 
 * 
 * @Entity public class Usuario {
 * 
 * @Id
 * 
 * @GeneratedValue(strategy = GenerationType.IDENTITY) private Long id;
 * 
 * @Column(name = "nome") private String nome;
 * 
 * 
 * @Column(name = "login") private String login;
 * 
 * 
 * @Column(name = "senha") private String senha;
 * 
 * 
 * @Column(name = "ativo") private boolean ativo;
 * 
 * 
 * @ManyToMany(cascade = CascadeType.ALL)
 * 
 * @JoinColumn(name="user_id_permissoes", unique = true) private List<Permissao>
 * permissao;
 * 
 * 
 * 
 * @ManyToMany(cascade = CascadeType.ALL)
 * 
 * @JoinColumn(name="user_id_grupos", unique = true) private List<Grupo> grupo;
 * 
 * 
 * 
 * public String getNome() { return nome; }
 * 
 * 
 * public void setNome(String nome) { this.nome = nome; }
 * 
 * 
 * public String getLogin() { return login; }
 * 
 * 
 * public void setLogin(String login) { this.login = login; }
 * 
 * 
 * public String getSenha() { return senha; }
 * 
 * 
 * public void setSenha(String senha) { this.senha = senha; }
 * 
 * 
 * public boolean isAtivo() { return ativo; }
 * 
 * 
 * public void setAtivo(boolean ativo) { this.ativo = ativo; }
 * 
 * 
 * 
 * 
 * 
 * public List<Permissao> getPermissao() { return permissao; }
 * 
 * 
 * public void setPermissao(List<Permissao> permissao) { this.permissao =
 * permissao; }
 * 
 * 
 * public List<Grupo> getGrupo() { return grupo; }
 * 
 * 
 * public void setGrupo(List<Grupo> grupo) { this.grupo = grupo; }
 * 
 * 
 * public Long getId() { return id; }
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * }
 */