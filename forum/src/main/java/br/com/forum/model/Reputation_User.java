package br.com.forum.model;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Reputation_User {

	@Id
	private Long id_user;

	@Column(name = "Score")
	private int score;
	
	@Column(name = "CreatedAt")
	private LocalDateTime createdAt;

	@Column(name = "UpdatedAt")
	private LocalDateTime updatedAt;

	@Column(name = "Enabled")
	private boolean enabled;

    @OneToOne(targetEntity = User.class, 
    cascade = {CascadeType.PERSIST, CascadeType.DETACH,CascadeType.MERGE,CascadeType.REFRESH} )
	private User user;



	public Reputation_User(int score, LocalDateTime createdAt,LocalDateTime updatedAt,
	 boolean enabled, User user) { 
		  this.score = score;
		  this.createdAt = createdAt;
		  this.updatedAt = updatedAt;
		  this.user = user;
	  }
	 

	 @PrePersist
	    private void onCreate(){
	        this.createdAt = LocalDateTime.now();
	    }
	

}
