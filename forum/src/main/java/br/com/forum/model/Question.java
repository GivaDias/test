package br.com.forum.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Question {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(targetEntity = User.class, 
    cascade = {CascadeType.PERSIST, CascadeType.DETACH,CascadeType.MERGE,CascadeType.REFRESH} )
    private List<User> id_user;
	
	@ManyToOne(targetEntity = Flag.class, 
	cascade = {CascadeType.PERSIST, CascadeType.DETACH,CascadeType.MERGE,CascadeType.REFRESH} )
	private List<Flag> id_flag;
	
	@Column(name = "Comment")
	private String comment;
	
	@Column(name = "Resolved")
	private boolean resolved;


	@Column(name = "CreatedAt")
	private LocalDateTime createdAt;

	@Column(name = "UpdatedAt")
	private LocalDateTime updatedAt;
	

	public Question(List<User> id_user,List<Flag> id_flag, String comment,
							boolean resolved, LocalDateTime createdAt, LocalDateTime updatedAt) { 
		  this.id_user = id_user;
		  this.id_flag = id_flag; 
		  this.comment = comment;
		  this.resolved = resolved;
		  this.createdAt = createdAt;
		  this.updatedAt = updatedAt;
	  
	  }

	 @PrePersist
	    private void onCreate(){
	        this.createdAt = LocalDateTime.now();
	    }
	

}
