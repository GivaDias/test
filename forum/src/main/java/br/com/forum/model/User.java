package br.com.forum.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "Name")
	private String name;
	
	@Column(name = "Email")
	private String email;
	
	@Column(name = "Birthdate")
	private LocalDate birthdate;

	@Column(name = "CreatedAt")
	private LocalDateTime createdAt;

	@Column(name = "UpdatedAt")
	private LocalDateTime updatedAt;

	@Column(name = "Enabled")
	private boolean enabled;

	
    @OneToMany(targetEntity = Role.class, 
    cascade = {CascadeType.PERSIST, CascadeType.DETACH,CascadeType.MERGE,CascadeType.REFRESH} )
	private List<Role> roles;


	   
	 public List<Role> getRoles() {
	        return roles;
	 }

	 public void setRoles(List<Role> roles) {
	        this.roles = roles;
	 }
	
	
	

	public User(String name, String email, LocalDate birthdate, LocalDateTime
	  createdAt,LocalDateTime updatedAt,boolean enabled, List<Role> role) { 
		  this.name = name;
		  this.email = email;
		  this.birthdate = birthdate; 
		  this.createdAt = createdAt;
		  this.updatedAt = updatedAt;
		  this.roles = role;
	  
	  }
	 

	 @PrePersist
	    private void onCreate(){
	        this.enabled = true;
	        this.createdAt = LocalDateTime.now();
	    }
	

}
