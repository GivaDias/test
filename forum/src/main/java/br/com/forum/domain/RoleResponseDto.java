package br.com.forum.domain;

import br.com.forum.model.Role;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class RoleResponseDto {

    
    private String description;
    

    public static RoleResponseDto transformaEmDTO(Role role) {
        return new RoleResponseDto(role.getDescription());
    }
}
