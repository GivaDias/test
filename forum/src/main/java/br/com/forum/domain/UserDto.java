package br.com.forum.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import br.com.forum.model.Role;
import br.com.forum.model.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {


	@NotBlank(message = "{name.not.blank}")
	private String name;
	
	@NotBlank(message = "{email.not.blank}")
	@Email(message = "{email.not.valid}")
	private String email;

	@NotNull(message ="{birthdate.not.blank}")
	private LocalDate birthdate;
	
	private LocalDateTime createdAt;
	
	private LocalDateTime updatedAt;

	private boolean enabled;
	
	private List<Role> role;


	
    public User transformaParaObjeto(){
        return new  User(name, email, birthdate, createdAt, updatedAt, enabled, role);
    }

}
