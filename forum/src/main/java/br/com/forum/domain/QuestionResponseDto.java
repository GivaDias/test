package br.com.forum.domain;

import br.com.forum.model.Question;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class QuestionResponseDto {

    
    private String name;
    

    public static QuestionResponseDto transformaEmDTO(Question question) {
        return new QuestionResponseDto(question.getComment() );
    }
}
