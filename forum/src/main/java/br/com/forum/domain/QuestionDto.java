package br.com.forum.domain;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.constraints.NotBlank;

import br.com.forum.model.Flag;
import br.com.forum.model.Question;
import br.com.forum.model.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuestionDto {

	private Long id; 
	
	@NotBlank(message = "{comment.not.blank}")
	private String comment;
	
	private boolean resolved;
	
	private LocalDateTime createdAt;
	
	private LocalDateTime updatedAt;

	private List<User> user;
	
	private List<Flag> flag;
	

	
    public Question transformaParaObjeto(){
        return new  Question(id ,user, flag, comment, resolved, createdAt, updatedAt);
    }

}
