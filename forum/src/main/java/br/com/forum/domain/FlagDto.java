package br.com.forum.domain;

import javax.validation.constraints.NotBlank;

import br.com.forum.model.Flag;
import lombok.Getter;

@Getter
public class FlagDto {

    @NotBlank(message = "{name.not.blank}")
    private String description;
    
    public Flag transformaParaObjeto(){
        return new Flag(description);
    }
}
