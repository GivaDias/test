package br.com.forum.domain;

import br.com.forum.model.User;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class UserResponseDto {

    
    private String name;
    

    public static UserResponseDto transformaEmDTO(User user) {
        return new UserResponseDto(user.getName());
    }
}
