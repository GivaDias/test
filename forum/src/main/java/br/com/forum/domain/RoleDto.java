package br.com.forum.domain;

import javax.validation.constraints.NotBlank;

import br.com.forum.model.Role;
import lombok.Getter;

@Getter
public class RoleDto {

    @NotBlank(message = "{name.not.blank}")
    private String description;
    
    public Role transformaParaObjeto(){
        return new Role(description);
    }
}
