package br.com.forum.domain;

import br.com.forum.model.Flag;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class FlagResponseDto {

    
    private String description;
    

    public static FlagResponseDto transformaEmDTO(Flag flag) {
        return new FlagResponseDto(flag.getDescription());
    }
}
