package br.com.forum.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.forum.model.Flag;

@Repository
public interface FlagRepository extends JpaRepository<Flag, Integer> {
	
    public Page<Flag> findAll(Pageable pageable);

	 @Query("Select f from Flag f where f.description = ?1")
	 Flag  findByDescription(@Param("description")String description);
	 

	 @Query("Select f from Flag f where f.id = ?1")
	 Flag  findByFlagId(@Param("id")long id);
	
	 
	 @Query("SELECT f FROM Flag f "
	            + "WHERE lower(description) like %:description% ")
	    public Page<Flag> findByDescriptionPagination(@Param("description") String description, Pageable pageable);

}
