package br.com.forum.repository;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.forum.model.Question;
import br.com.forum.model.User;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
	
    public Page<Question> findAll(Pageable pageable);

		
	 @Query("Select u from Question u where u.comment = ?1")
	 User  findByName(@Param("comment")String comment);
	 
		
	 @Query("Select u from Question u where u.id = ?1")
	 Question  findByIdQuestion(@Param("id")Long id);
	
	 
	 @Modifying
	 @Query("delete from User c where c.name=:name")
	 void delete(@Param("name") String  name);
		
	 @Query(value = "delete from user_roles where users_id= :user_id", nativeQuery = true)
	 void deleteQuestion(@Param("user_id") Long user_id);
	 
	 @Query("SELECT u FROM User u "
	            + "WHERE lower(name) like %:name%")
	    public Page<User> findByNamePagination(@Param("name") String name,   Pageable pageable);

	 @Query("SELECT u FROM User u "
	            + "WHERE lower(email) like %:email%")
	    public Page<User> findByEmailPagination(@Param("email") String email,   Pageable pageable);

	 
	    @Modifying
		@Query(value = "INSERT INTO Question(resolved, created_At, comment, id_flag_id, id_user_id ) "
				+ " VALUES (:resolved, :createdAt, :comment,:id_flag_id,:id_user_id)", nativeQuery = true)
		@Transactional
		Integer saveQuestion(@Param("resolved") boolean  resolved, @Param("createdAt") LocalDateTime createdAt, @Param("comment") String comment, 
	    @Param("id_flag_id") long id_flag_id, @Param("id_user_id") long id_user_id);

	    @Modifying
		@Query("update Question c set c.resolved= :resolved , c.updatedAt= :updated_At,  c.comment= :comment  WHERE id = :id")
		@Transactional
		void updateQuestion(@Param("resolved") boolean  resolved, @Param("updated_At") LocalDateTime updated_At, @Param("comment") String comment, 
	    @Param("id") Long id);

	    
	 
		/*
		 * @Query("SELECT u FROM User u " + "WHERE birthdate like %:birthdate%") public
		 * Page<User> findByBirthdatePagination(@Param("birthdate") LocalDate birthdate,
		 * Pageable pageable);
		 */
	 @Query(value = "SELECT * FROM User  WHERE birthdate =:birthdate", nativeQuery = true)
	 public Page<User> findByBirthdatePagination(LocalDate birthdate,Pageable pageable );
	 
}
