package br.com.forum.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.forum.model.Role;

@Repository
public interface RoleRepository  extends JpaRepository<Role, Long> {
	
    public Page<Role> findAll(Pageable pageable);

		
	 @Query("Select r from Role r where r.description = ?1")
	 Role  findByDescription(@Param("description")String description);
	 
	 @Query("Select r from Role r where r.id = ?1")
	 Role  findByRoleId(@Param("id")long id);
	
	 
	 @Query("SELECT r FROM Role r "
	            + "WHERE lower(description) like %:description% ")
	    public Page<Role> findByDescriptionPagination(@Param("description") String description, Pageable pageable);

}
