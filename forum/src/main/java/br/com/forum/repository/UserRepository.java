package br.com.forum.repository;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.forum.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
    public Page<User> findAll(Pageable pageable);

		
	 @Query("Select u from User u where u.name = ?1")
	 User  findByName(@Param("name")String name);
	 
	 @Query("Select u from User u where u.id = ?1")
	 User  findByUserId(@Param("id")long id);
	
	 @Modifying
	 @Query("delete from User c where c.name=:name")
	 void delete(@Param("name") String  name);
		
	 @Query(value = "delete from user_roles where users_id= :user_id", nativeQuery = true)
	 void deleteRelation(@Param("user_id") Long user_id);
	 
	 @Query("SELECT u FROM User u "
	            + "WHERE lower(name) like %:name%")
	    public Page<User> findByNamePagination(@Param("name") String name,   Pageable pageable);

	 @Query("SELECT u FROM User u "
	            + "WHERE lower(email) like %:email%")
	    public Page<User> findByEmailPagination(@Param("email") String email,   Pageable pageable);

		
	 @Query(value = "SELECT * FROM User  WHERE birthdate =:birthdate", nativeQuery = true)
	 public Page<User> findByBirthdatePagination(LocalDate birthdate,Pageable pageable );
	 
}
