package br.com.forum.controller;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.forum.domain.RoleDto;
import br.com.forum.domain.RoleResponseDto;
import br.com.forum.exception.ApiError;
import br.com.forum.model.Role;
import br.com.forum.repository.PageableFactory;
import br.com.forum.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags = "Role", description = "API Forum")
@RequestMapping(value = "/role")
public class RoleController {

	private final RoleService roleService;

	@Autowired
	public RoleController(RoleService roleService) {
		this.roleService = roleService;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation(value = "Create of roles")
	@PostMapping
	public ResponseEntity<RoleResponseDto> create(@RequestBody @Valid RoleDto dto) {
		Role role = roleService.findByDescription(dto.getDescription());
		if (role == null) {
			role = roleService.create(dto.transformaParaObjeto());
		}
		if (role == null) {
			return ApiError.badRequest("Error on create");
		}
		return new ResponseEntity<>(RoleResponseDto.transformaEmDTO(role), HttpStatus.CREATED);
	}

	@ApiOperation(value = "Delete of roles")
	@DeleteMapping
	public ResponseEntity<Object> delete(@RequestParam(value = "description", required = true) String description) {
		Role role = roleService.findByDescription(description);
		if (role != null) {
			roleService.deleteRole(role.getId());
			return new ResponseEntity<>(HttpStatus.MOVED_PERMANENTLY);
		} else
			return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
	}

	@ApiOperation(value = "List of roles")
	@GetMapping()
	public Page<Role> list(@RequestParam(value = "page", required = false, defaultValue = "0") int page,
			@RequestParam(value = "size", required = false, defaultValue = "10") int size,
			@RequestParam(value = "sort", required = false) String sort,
			@RequestParam(value = "description", required = false) String q) {
		Pageable pageable = new PageableFactory(page, size, sort).getPageable();

		Page<Role> resultPage;

		if (q == null) {
			resultPage = roleService.findAll(pageable);
		} else {
			resultPage = roleService.findByDescriptionPagination(q.toLowerCase(), pageable);
		}

		return resultPage;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation(value = "Update roles")
	@PutMapping(value = "/{id}")
	public ResponseEntity<Role> put(@PathVariable("id") @Valid Integer id, @RequestBody @Valid RoleDto dto) { 
		try {
			Optional<Role> currentRole = Optional.ofNullable(roleService.findByRoleId(id));
			if (!currentRole.isPresent()) {
				return ApiError.internalServerError("Role not found  "+ HttpStatus.NO_CONTENT);
			}
			currentRole.get().setDescription(dto.getDescription());
			currentRole.get().setUpdatedAt(LocalDateTime.now());
				try {
					Role roleUpdated = roleService.create(currentRole.get());
				 return new ResponseEntity<>(roleUpdated, HttpStatus.CREATED);
				} catch (Exception e) {
			     return ApiError.internalServerError("Error update role"+ HttpStatus.UNPROCESSABLE_ENTITY);
				}
        } catch (Exception e) {
        	return ApiError.internalServerError("Error select role "+ HttpStatus.UNPROCESSABLE_ENTITY);
        }
	}
}
