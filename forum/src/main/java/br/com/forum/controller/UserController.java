package br.com.forum.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.forum.domain.UserDto;
import br.com.forum.domain.UserResponseDto;
import br.com.forum.exception.ApiError;
import br.com.forum.model.User;
import br.com.forum.repository.PageableFactory;
import br.com.forum.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags = "User", description = "API Forum")
@RequestMapping(value = "/user")
public class UserController {

	private final UserService userService;

	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}
	

	@SuppressWarnings("unchecked")
	@ApiOperation(value = "Create of User")
	@PostMapping
	public ResponseEntity<UserResponseDto> create(@RequestBody @Valid UserDto dto) {
		User user = userService.findByName(dto.getName());
		if (user == null) {
			user = userService.create(dto.transformaParaObjeto());
		}
		if (user == null) {
			return ApiError.badRequest("Error on create");
		}
		return new ResponseEntity<>(UserResponseDto.transformaEmDTO(user), HttpStatus.CREATED);
	}
	
	
	@ApiOperation(value = "Delete User")
	@DeleteMapping
	public ResponseEntity<Object> delete(@RequestParam(value = "name", required = true) String name) {
		User user = userService.findByName(name);
		if (user != null) {
			userService.deleteUser(user.getId());
			return new ResponseEntity<>(HttpStatus.MOVED_PERMANENTLY);
		} else
			return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
	}

	@ApiOperation(value = "List of roles")
	@GetMapping()
	public Page<User> list(@RequestParam(value = "page", required = false, defaultValue = "0") int page,
			@RequestParam(value = "size", required = false, defaultValue = "10") int size,
			@RequestParam(value = "sort", required = false) String sort,
			@RequestParam(value = "name", required = false) String name, 
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "birthdate", required = false) String birthdate){
		Pageable pageable = new PageableFactory(page, size, sort).getPageable();

		Page<User> resultPage = null;

		if ((name == null) && (email == null) && (birthdate == null) ) {
			resultPage = userService.findAll(pageable);
		} else if(name != null ) {
			resultPage = userService.findByNamePagination(name.toLowerCase(), pageable);
		} else if(email != null) {
			resultPage = userService.findByEmailPagination(email.toLowerCase(), pageable);
		} else if(birthdate != null) {
			LocalDate birth = LocalDate.parse(birthdate) ;
			resultPage = userService.findByBirthdatePagination(birth, pageable);
		}

		return resultPage;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation(value = "Update user")
	@PutMapping(value = "/{id}")
	public ResponseEntity<User> put(@PathVariable("id") @Valid Integer id, @RequestBody @Valid UserDto dto) { 
		try {
			Optional<User> currentUser = Optional.ofNullable(userService.findByUserId(id));
			if (!currentUser.isPresent()) {
				return ApiError.internalServerError("User not found  "+ HttpStatus.NO_CONTENT);
			}
			currentUser.get().setName(dto.getName());
			currentUser.get().setBirthdate(dto.getBirthdate());
			currentUser.get().setEnabled(dto.isEnabled());
			currentUser.get().setRoles(dto.getRole());
			currentUser.get().setUpdatedAt(LocalDateTime.now());
				try {
					User userUpdated = userService.create(currentUser.get());
				 return new ResponseEntity<>(userUpdated, HttpStatus.CREATED);
				} catch (Exception e) {
			     return ApiError.internalServerError("Error update user"+ HttpStatus.UNPROCESSABLE_ENTITY);
				}
        } catch (Exception e) {
        	return ApiError.internalServerError("Error select user "+ HttpStatus.UNPROCESSABLE_ENTITY);
        }
	}
}
