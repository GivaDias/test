package br.com.forum.controller;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.forum.domain.FlagDto;
import br.com.forum.domain.FlagResponseDto;
import br.com.forum.exception.ApiError;
import br.com.forum.model.Flag;
import br.com.forum.repository.PageableFactory;
import br.com.forum.service.FlagService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags = "Flag", description = "API Forum")
@RequestMapping(value = "/flag")
public class FlagController {
	 
	private final FlagService flagService;

    @Autowired
    public FlagController(FlagService flagService) {
        this.flagService = flagService;
    }
    

    @PostMapping
    public ResponseEntity<FlagResponseDto> create(@RequestBody @Valid FlagDto dto) {
    	Flag flag = flagService.findByDescription(dto.getDescription());
    	if(flag == null) {
    		flag = flagService.create(dto.transformaParaObjeto());
    	 }
        return new ResponseEntity<>(FlagResponseDto.transformaEmDTO(flag), HttpStatus.CREATED);
    }	

    
    @DeleteMapping
	public ResponseEntity<Object> delete(@RequestParam(value = "description",required = true) String description ) {
		 Flag flag = flagService.findByDescription(description);
		if (flag != null ) {
			flagService.delete(flag);
			return new ResponseEntity<>(HttpStatus.MOVED_PERMANENTLY);
		} else
			return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
	}
    
    @ApiOperation(value = "List of flags")
    @GetMapping()
    public Page<Flag> list(
            @RequestParam(
                    value = "page",
                    required = false,
                    defaultValue = "0") int page,
            @RequestParam(
                    value = "size",
                    required = false,
                    defaultValue = "10") int size,
            @RequestParam(
                    value = "sort",
                    required = false) String sort,
            @RequestParam(
                    value = "description",
                    required = false) String q
    ) {
        Pageable pageable = new PageableFactory(page, size, sort).getPageable();

        Page<Flag> resultPage;

        if (q == null) {
            resultPage = flagService.findAll(pageable);
        } else {
            resultPage = flagService.findByDescriptionPagination(q.toLowerCase(), pageable);
        }
        return resultPage;
    }
	
    @SuppressWarnings("unchecked")
	@ApiOperation(value = "Update Flag")
	@PutMapping(value = "/{id}")
	public ResponseEntity<Flag> put(@PathVariable("id") @Valid Integer id, @RequestBody @Valid FlagDto dto) { 
		try {
			Optional<Flag> currentFlag = Optional.ofNullable(flagService.findByFlagId(id));
			if (!currentFlag.isPresent()) {
				return ApiError.internalServerError("Flag not found  "+ HttpStatus.NO_CONTENT);
			}
				currentFlag.get().setDescription(dto.getDescription());
				currentFlag.get().setUpdatedAt(LocalDateTime.now());
				try {
					Flag flagUpdated = flagService.create(currentFlag.get());
				 return new ResponseEntity<>(flagUpdated, HttpStatus.CREATED);
				} catch (Exception e) {
			     return ApiError.internalServerError("Error update flag"+ HttpStatus.UNPROCESSABLE_ENTITY);
				}
        } catch (Exception e) {
        	return ApiError.internalServerError("Error select flag "+ HttpStatus.UNPROCESSABLE_ENTITY);
        }
	}

    
}
