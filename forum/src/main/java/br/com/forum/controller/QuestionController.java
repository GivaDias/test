package br.com.forum.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.forum.domain.QuestionDto;
import br.com.forum.domain.QuestionResponseDto;
import br.com.forum.exception.ApiError;
import br.com.forum.model.Question;
import br.com.forum.service.QuestionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags = "Question", description = "API Forum")
@RequestMapping(value = "/question")
public class QuestionController {

	private final QuestionService questionService;

	@Autowired
	public QuestionController(QuestionService questionService) {
		this.questionService = questionService;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation(value = "Create of User")
	@PostMapping
	public ResponseEntity<QuestionResponseDto> create(@RequestBody @Valid QuestionDto dto) {
		    Question question = new Question();
		    if(dto.getUser() == null) {
		    	return ApiError.internalServerError("Id user can´t null"+ HttpStatus.NO_CONTENT);
		    }else {
		    	question.setId_user(dto.getUser());
		    }
		    if(dto.getFlag() == null) {
		    	return ApiError.internalServerError("Id flag can´t null"+ HttpStatus.NO_CONTENT);
		    }else {
		    	question.setId_flag(dto.getFlag());
		    }
		    if(dto.getComment() == null) {
		    	return ApiError.internalServerError("Comment can´t null"+ HttpStatus.NO_CONTENT);
		    }else {
		    	question.setComment(dto.getComment());
		    }
		    
		    Integer teste  = questionService.create(question);
		if (teste == 0) {
			return ApiError.badRequest("Error on create");
		}
		return new ResponseEntity<>(QuestionResponseDto.transformaEmDTO(question), HttpStatus.CREATED);
	}
	
	@SuppressWarnings("unchecked")
	@ApiOperation(value = "Update user")
	@PutMapping(value = "/{id}")
	public ResponseEntity<Question> put(@PathVariable("id") @Valid Long id, @RequestBody @Valid QuestionDto dto) { 
		try {
		    Question question = new Question();

			Optional<Question> currentUser = Optional.ofNullable(questionService.findByQuestionId(id));
			if (!currentUser.isPresent()) {
				return ApiError.internalServerError("User not found  "+ HttpStatus.NO_CONTENT);
			}
			if(dto.getUser() == null) {
		    	return ApiError.internalServerError("Id user can´t null"+ HttpStatus.NO_CONTENT);
		    }else {
		    	question.setId_user(dto.getUser());
		    }
		    if(dto.getFlag() == null) {
		    	return ApiError.internalServerError("Id flag can´t null"+ HttpStatus.NO_CONTENT);
		    }else {
		    	question.setId_flag(dto.getFlag());
		    }
		    if(dto.getComment() == null) {
		    	return ApiError.internalServerError("Comment can´t null"+ HttpStatus.NO_CONTENT);
		    }else {
		    	question.setComment(dto.getComment());
		    }
		    
				try {
					Question questionUpdated = questionService.updateQuestion(question);
				 return new ResponseEntity<>(questionUpdated, HttpStatus.CREATED);
				} catch (Exception e) {
			     return ApiError.internalServerError("Error update user"+ HttpStatus.UNPROCESSABLE_ENTITY);
				}
        } catch (Exception e) {
        	return ApiError.internalServerError("Error select user "+ HttpStatus.UNPROCESSABLE_ENTITY);
        }
	}

	
	
/*	@ApiOperation(value = "Delete User")
	@DeleteMapping
	public ResponseEntity<Object> delete(@RequestParam(value = "name", required = true) String name) {
		User user = userService.findByName(name);
		if (user != null) {
			userService.deleteUser(user.getId());
			return new ResponseEntity<>(HttpStatus.MOVED_PERMANENTLY);
		} else
			return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
	}

	@ApiOperation(value = "List of roles")
	@GetMapping()
	public Page<User> list(@RequestParam(value = "page", required = false, defaultValue = "0") int page,
			@RequestParam(value = "size", required = false, defaultValue = "10") int size,
			@RequestParam(value = "sort", required = false) String sort,
			@RequestParam(value = "name", required = false) String name, 
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "birthdate", required = false) String birthdate){
		Pageable pageable = new PageableFactory(page, size, sort).getPageable();

		Page<User> resultPage = null;

		if ((name == null) && (email == null) && (birthdate == null) ) {
			resultPage = userService.findAll(pageable);
		} else if(name != null ) {
			resultPage = userService.findByNamePagination(name.toLowerCase(), pageable);
		} else if(email != null) {
			resultPage = userService.findByEmailPagination(email.toLowerCase(), pageable);
		} else if(birthdate != null) {
			LocalDate birth = LocalDate.parse(birthdate) ;
			resultPage = userService.findByBirthdatePagination(birth, pageable);
		}

		return resultPage;
	}*/

	}