/*
 * www.paulocollares.com.br
 */
package br.com.forum.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Classe responsável para gerar os erros padronizados
 * @author Paulo Collares
 */
public class ApiError {

    public static ResponseEntity badRequest(String message) {
        return error(message, HttpStatus.BAD_REQUEST);
    }

    public static ResponseEntity notFound(String message) {
        return error(message, HttpStatus.NOT_FOUND);
    }

    public static ResponseEntity internalServerError(String message) {
        return error(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static ResponseEntity unauthorized(String message) {
        return error(message, HttpStatus.UNAUTHORIZED);
    }

    public static ResponseEntity error(String message, HttpStatus httpStatus) {
        Map<Object, Object> model = new HashMap<>();
        model.put("error", httpStatus.getReasonPhrase());
        model.put("status", httpStatus.value());
        model.put("message", message);

        return new ResponseEntity(model, httpStatus);
    }

}
