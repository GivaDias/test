/*
 * package br.com.manager.controller;
 * 
 * import static io.restassured.module.mockmvc.RestAssuredMockMvc.given; import
 * static io.restassured.module.mockmvc.RestAssuredMockMvc.standaloneSetup;
 * import static org.mockito.Mockito.when;
 * 
 * import java.util.ArrayList; import java.util.List;
 * 
 * import org.junit.jupiter.api.BeforeEach; import org.junit.jupiter.api.Test;
 * //import org.junit.runner.RunWith; import
 * org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest; import
 * org.springframework.boot.test.mock.mockito.MockBean; //import
 * org.springframework.security.core.userdetails.UserDetailsService; import
 * org.springframework.test.context.ContextConfiguration; import
 * org.springframework.test.context.junit4.SpringJUnit4ClassRunner; import
 * org.springframework.test.context.web.WebAppConfiguration; import
 * org.springframework.test.web.servlet.MockMvc; import
 * org.springframework.test.web.servlet.setup.MockMvcBuilders; import
 * org.springframework.web.context.WebApplicationContext;
 * 
 * import com.fasterxml.jackson.databind.ObjectMapper;
 * 
 * //import br.com.manager.MyUserDetailsService; import
 * br.com.manager.domain.CustomerDto; import br.com.manager.model.Address;
 * import br.com.manager.service.ICustomerService; import
 * io.restassured.http.ContentType;
 * 
 * 
 * //@RunWith(SpringJUnit4ClassRunner.class)
 * 
 * @ContextConfiguration
 * 
 * @WebAppConfiguration //added this annotation
 * 
 * @WebMvcTest public class CustomerControllerTest {
 * 
 * @Autowired private CustomerController customerController;
 * 
 * @MockBean private ICustomerService service;
 * 
 * @Autowired private WebApplicationContext webApplicationContext; private
 * MockMvc mockMvc;
 * 
 * 
 * 
 * @MockBean private UserDetailsService userDetailsService;
 * 
 * @MockBean private MyUserDetailsService myUserDetailsService;
 * 
 * @BeforeEach public void setUp() { mockMvc =
 * MockMvcBuilders.webAppContextSetup(webApplicationContext) .build();
 * 
 * standaloneSetup(this.customerController);
 * //standaloneSetup(this.userDetailsService); }
 * 
 * @Test public void getAllCustomer() {
 * 
 * List<CustomerDto> listCustomer = new ArrayList<CustomerDto>(); CustomerDto
 * customerDto = new CustomerDto(); customerDto.setCpf(123);
 * customerDto.setName("Jose"); customerDto.setId(1L);
 * 
 * Address address = new Address(); address.setBairro("Centro");
 * address.setCep("(686868"); address.setCidade("São Paulo");
 * address.setComplemento("Apto 7"); address.setEstado("São Paulo");
 * address.setLogradouro("Rua Nogueira"); address.setNumero("566565");
 * 
 * customerDto.setAddress(address); listCustomer.add(customerDto);
 * 
 * when(this.service.listCustomer()).thenReturn(listCustomer);
 * 
 * given() .accept(ContentType.JSON) .when() .get("customer/all") .then()
 * .statusCode(org.springframework.http.HttpStatus.OK.value());
 * 
 * 
 * 
 * }
 * 
 * }
 */